import { describe, it } from 'mocha';
import sinon from 'sinon';
import { expect } from 'chai';

import { Authenticate } from './authenticate';
import { RequestError, AuthenticationError } from '../utils/app-errors';

const sandbox = sinon.createSandbox();

describe('authenticate', async () => {
  it('should pass error to next middleware if authorization header does not exist', async () => {
    const req = { headers: {} };
    const next = sandbox.spy();
    const middleware = Authenticate({} as any)();

    await middleware(req as any, {} as any, next);

    expect(next.args[0][0]).to.be.instanceOf(RequestError);
    expect(next.args[0][0].message).to.be.equals('Token is not provided');
  });

  it('should pass error to next middleware if service throws error', async () => {
    const req = { headers: { authorization: 'Bearer token' } };
    const next = sandbox.spy();
    const usersService = {
      authenticateUser: (): void => {
        throw new Error();
      },
    };

    const middleware = Authenticate(usersService as any)();
    await middleware(req as any, {} as any, next);

    expect(next.args[0][0]).to.be.instanceOf(AuthenticationError);
  });

  it('should authenticate user', async () => {
    const req = { headers: { authorization: 'Bearer token' } };
    const next = sandbox.spy();
    const usersService = { authenticateUser: (): void => {} };

    const middleware = Authenticate(usersService as any)();
    await middleware(req as any, {} as any, next);

    expect(next.args[0][0]).to.be.undefined;
    expect(next.called).to.be.true;
  });
});
