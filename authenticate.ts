import { NextFunction, Request, Response } from 'express';

import { RequestError, AuthenticationError } from '../utils/app-errors';
import { usersService, UsersService } from '../components/users/users.service'; //user componts
import { lang } from '../utils/lang'; // hare we create lang file with custom errors

export function Authenticate(usersService: UsersService) {
  return function authenticate() {
    /**
     * Authentication middleware. Checks if user token is valid and attaches authenticated user to request.
     * Passes error to next middleware on error.
     */
    return async function (req: Request, _res: Response, next: NextFunction): Promise<void> {
      if (req.headers.authorization === undefined) {
        next(new RequestError(lang['DE'].token_is_not_provided));
        return;
      }
      const [, token] = req.headers.authorization.split(' ');
      let authUser;
      try {
        authUser = await usersService.authenticateUser(token);
      } catch (error) {
        // next(new AuthenticationError('Invalid token'));
        next(new AuthenticationError(lang['DE'].invalid_token));
      }
      req.user = authUser;
      next();
    };
  };
}

export const authenticate = Authenticate(usersService);
